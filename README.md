<h1 align="center">Please follow the steps to build the same code for different platforms and user types</h1>

### Teacher {

- id: com.tech.teacher,
- name: ITC Teacher

### }

### Student {

- id: com.tech.student,
- name: ITC Student

### }

<h3 align="center">Follow the steps for build according to user type</h3>

### **-----------------------------------Build Teacher----------------------------------**

- _Step1_: Change Package _name_ and Package _id_ in _ionic.config.json_
- _Step2_: Change _appId_ and _appName_ in _capacitor.config.ts_
- _Step3_: Goto _Login_ Page and set _Type_ and _Route_ to _teacher_
- _Step4_: Goto _ParentsProfile_ Page and set _Type_ and _Route_ to _teacher_

### **----------------------------------------------------------------------------------**

### **-----------------------------------Build Student----------------------------------**

- _Step1_: Change Package _name_ and Package _id_ in _ionic.config.json_
- _Step2_: Change _appId_ and _appName_ in _capacitor.config.ts_
- _Step3_: Goto _Login_ Page and set _Type_ and _Route_ to _student_
- _Step4_: Goto _ParentsProfile_ Page and set _Type_ and _Route_ to _student_

### **----------------------------------------------------------------------------------**
