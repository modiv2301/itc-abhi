// This a service worker file for receiving push notifitications.
// See `Access registration token section` @ https://firebase.google.com/docs/cloud-messaging/js/client#retrieve-the-current-registration-token

// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');


// Your web app's Firebase configuration

const firebaseConfig = {
    apiKey: "AIzaSyALTAJlb9aWj54e23vga3mMOM30CCBOQcQ",
    authDomain: "itcfirebase-f8f3a.firebaseapp.com",
    projectId: "itcfirebase-f8f3a",
    storageBucket: "itcfirebase-f8f3a.appspot.com",
    messagingSenderId: "1087446524858",
    appId: "1:1087446524858:web:b1eea5e1dcbbf5f69c7383"
  };
  

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

// Handle incoming messages while the app is not in focus (i.e in the background, hidden behind other tabs, or completely closed).
messaging.onBackgroundMessage(function(payload) {
  console.log('Received background message ', payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});