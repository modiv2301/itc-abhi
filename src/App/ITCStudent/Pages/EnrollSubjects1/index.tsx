import { IonButton } from "@ionic/react";
import { Typography } from "@mui/material";
import React ,{useState,useEffect}from "react";
import { useHistory } from "react-router";
import { AuthPage, LanguageBox } from "../../../../components";
import { LanguageOptions } from "../../../../components/LanguageBox";
import "./style.scss";
// import { SideMenu } from "App/ITCTeacher/Components";
import { useLocation} from "react-router-dom";
import { getEnrollSubject } from "api/api";
import { getPackageCategory } from '../../../../api/api';
import { SideMenu } from "App/ITCStudent/Components";

const languages: Array<LanguageOptions[]> = 
[
  [
    {
      name: "BHASA MELAYU",
      time: "7PM - 8PM",
      day: "WEDNESDAY",
      checked:false,
    },
  ],
  [
    {
      name: "ENGLISH",
      time: "8PM - 9PM",
      day: "TUESDAY",
      checked:false,
    },
  ],
  [
    {
      name: "MATHEMATICS",
      time: "6PM - 7PM",
      day: "TUESDAY",
      checked:false,
    },
  ],
  [
    {
      name: "SCIENCE(ENGLISH)",
      time: "6PM - 7PM",
      day: "TUESDAY",
      checked:false,
    },
    {
      name: "SAINS(B.MELAYU)",
      time: "6PM - 7PM",
      day: "TUESDAY",
      checked:false,
    },
  ],
  [
    {
      name: "SEJARAH",
      time: "6PM - 7PM",
      day: "THURSDAY",
      checked:false,
    },
  ],
  [
    {
      name: "GEOGRAFI",
      time: "6PM - 7PM",
      day: "THURSDAY",
      checked:false,
    },
  ],
  [
    {
      name: "BHASA TAMIL",
      time: "6PM - 7PM",
      day: "MONDAY",
      checked:false,
    },
  ],
];

// author abhishek singh
//for dynamic data comment 13 to 78 line no
//uncomment line no 91
//uncomment line no 126-133 



export const EnrollSubjects1 = (props) => {

  const [total, settotal] = useState(0);
  const history = useHistory();
  const location = useLocation();
  const [checkedState, setCheckedState] = useState([]);
  const amount=location.state;
  // const [languages, setlanguages] = useState([]);

  var totaltrue=0;
  
    const handleCheck = (event:number,index:number) => {
    
    languages[event][index].checked=!languages[event][index].checked
    languages.map((item)=>{
      if(item.length<=1){    
        if(item[0].checked) {
          totaltrue=totaltrue+1;
          settotal(totaltrue)
        }      
      }else{
        var data=item;
        data.map((value)=>{
          if(value.checked) {
            totaltrue=totaltrue+1;
            settotal(totaltrue)
          } 
        }) 
      }         
    })
    };

    useEffect(()=> {
      // getEnrollSubject(amount.package,amount.category,).then(res => {
      //   const {data, status} = res;
      //   if(status) {
      //     console.log(data);
      //     setlanguages(data.data)
      //   }
      //   setLoader(false)
      // })
    }, [])
     

  return (
    <>
    
    <SideMenu/>
  
    <AuthPage title="Combo Package" showBackButton containerWidth="md">
      <p className="subjectHeading">Please Select your preferred subjects</p>
      
      <div>
        {languages.map((language,indexdata) => (
          <LanguageBox
            title={language.length > 1 ? "SCIENCE" : undefined}
            languages={language}
            indexdata={indexdata}
            handleCheck={handleCheck}
          />
        ))}
      </div>
      
      <div style={{ marginTop: "40px", textAlign: "center" }}>
        <h2>Total Amount : {total==1 ? amount['single']*total :amount['multiple']*total }  </h2>
        <IonButton
          onClick={() =>{
            history.push({
               pathname: "/s/payment",
               state:{
                totalAmount:total==1 ? amount['single']*total :amount['multiple']*total
                }
           });
         }}
          
          className="continue"
          shape="round"
          color="secondary"
        >
          Pay Now
        </IonButton>
      </div>
    </AuthPage>
    </>
  );
};

function setLoader(arg0: boolean) {
  throw new Error("Function not implemented.");
}

