import { IonButton } from '@ionic/react';
import { Stack, Typography } from '@mui/material';
import { getPdfList } from 'api/api';
import React, { useEffect, useState } from 'react';
import { AuthPage } from '../../../../components';
import { style } from './../../../../Utility';
import CircularProgress from '@mui/material/CircularProgress';
import { useHistory } from "react-router";
// import { SideMenu } from "App/ITCTeacher/Components";
import { SideMenu } from 'App/ITCStudent/Components';

export const NotesList = (props):any => {
  
  const history = useHistory();
  const [subjectList, setSubjectList] = useState([]);
  const [loader, setLoader] = useState(true);
  const [subjectId, setSubjectId] = useState(props.match.params.subjectId)
  
  // console.log(props.match.params.subjectId,"propsssssssss");

  useEffect(()=> {
    getPdfList(subjectId).then(res => {
      const {data, status} = res;
      if(status) {
        setSubjectList(data.data)
      }
      setLoader(false)
    })
  }, [])

  const downloadPdf = (link) => {
    //  window.location.href=link
    window.open(link, '_blank', 'noopener,noreferrer');
  }
  
  console.log(subjectList, "my subject list download")
  
  if(loader)
    return <CircularProgress />
  return (
    <>
    <SideMenu/>    


    <AuthPage containerWidth='sm' title='Notes' showBackButton>
      <div
        className='ion-padding'
        style={{ display: 'flex', justifyContent: 'center' }}
      >
        <div style={{ width: style.maxWidth }}>
          <p className='heading'>Please select your preferred subjcet</p>
          <Stack sx={{ alignItems: 'center' }} spacing={2}>
            {subjectList.map((language) => (
              <>
              
              <IonButton
                style={{ minWidth: 'calc(100% - 40px)', height: '47px' }}
                shape='round'
                color='primary'
                expand='block'
                onClick={()=>downloadPdf(language.file_name)}
              >
                {language.title}
              </IonButton>
              </>
            ))}
          </Stack>
        </div>
      </div>
    </AuthPage>
    </>
  );
};
