import './index.scss';
import { Box, Typography } from '@mui/material';
import React from 'react';
import { AuthPage } from '../../../../components';
import { IonButton } from '@ionic/react';
import './index.scss';
// import { SideMenu } from 'App/ITCTeacher/Components';
import { useHistory } from "react-router";
import { useLocation} from "react-router-dom";
import { SideMenu } from 'App/ITCStudent/Components';

export const Payment = () => {

  const location = useLocation();

  const history = useHistory();
  
  const totalAmount=location.state;

  // console.log(totalAmount['totalAmount']);
  

  const Paymentform = () =>{
    return(
        <>
            {/* <form method="post" action="https://www.mobile88.com/epayment/entry.asp" name="ePayment">
                           <input type="hidden" name="MerchantCode" value="M00003"/>
                            <input type="hide" name="PaymentId" value=""/>
                            <input type="hide" name="RefNo" value="A00000001"/>                   
                            <input type="hide" name="Amount" value="1.00"/>
                            <input type="hide" name="Currency" value="MYR"/>
                            <input type="hide" name="ProdDesc" value="Photo Print"/>
                            <input type="hide" name="UserName" value="John Tan"/>
                            <input type="hide" name="UserEmail" value="john@hotmail.com"/>
                            <input type="hide" name="UserContact" value="0126500100"/>
                            <input type="hide" name="Remark" value=""/>
                            <input type="hide" name="Lang" value="UTF-8"/>
                            <input type="hide" name="SignatureType" value="SHA256"/>
                            <input type="hidden" name="Signature" value="b81af9c4048b0f6c447129f0f5c0eec8d67cbe19eec26f2cdaba5df4f4dc5a28"/>
            </form> */}
        </>
    )
  }


  return (

    <>
    <SideMenu/>
    <AuthPage title='Payment' showBackButton containerWidth='sm'>
      <div className='ion-padding payment-page'>
        <Typography className='color-primary'>HASHINI SHIVAGURU</Typography>
        <Typography className='formName'>FORM 3</Typography>
        <div>
          <div>
            <Box className={`ion-padding status-root-warning`}>
              <Box>
                <Typography
                  sx={{ color: 'black' }}
                  className={`status-heading background-warning`}
                >
                  PAYMENT DETAILS
                </Typography>
                <div className='ion-padding payment-calc'>
                  <div className='payment'>
                    <p>Original:</p>
                    <span>RM 
                      {totalAmount['totalAmount']}
                    </span>
                  </div>
                  <div className='payment'>
                    <p>Discounted:</p>
                    <span>RM 0.00</span>
                  </div>
                  <div className='total-pay'></div>
                </div>
                <Typography className='total'>Total: RM {totalAmount['totalAmount']}</Typography>
              </Box>
            </Box>
          </div>
        </div>

        <form method="post" action="https://www.mobile88.com/epayment/entry.asp" name="ePayment">
        <div className='online'>
                            <input type="hidden" name="MerchantCode" value="M00003"/>
                            <input type="hidden" name="PaymentId" value=""/>
                            <input type="hidden" name="RefNo" value="A00000001"/>                   
                            <input type="hidden" name="Amount" value="1.00"/>
                            <input type="hidden" name="Currency" value="MYR"/>
                            <input type="hidden" name="ProdDesc" value="Photo Print"/>
                            <input type="hidden" name="UserName" value="John Tan"/>
                            <input type="hidden" name="UserEmail" value="john@hotmail.com"/>
                            <input type="hidden" name="UserContact" value="0126500100"/>
                            <input type="hidden" name="Remark" value=""/>
                            <input type="hidden" name="Lang" value="UTF-8"/>
                            <input type="hidden" name="SignatureType" value="SHA256"/>
                            <input type="hidden" name="Signature" value="b81af9c4048b0f6c447129f0f5c0eec8d67cbe19eec26f2cdaba5df4f4dc5a28"/>
          <IonButton color='secondary' shape='round' type="submit">
            Online Payment
          </IonButton>

          <Typography className={`onlineText`}>
            Payment can be made via FPX and payment status will be updated
            immediately
          </Typography>
        </div>
        </form>

        <div className='bankdetails'>
          <Typography className={`bankName`}>Bank Account Details</Typography>
          <Typography className='color-tertiary training'>
            ITC INTELLECT TRAINING
          </Typography>
          <Typography className='color-tertiary bankId'>
            546454354345 - MYBANK
          </Typography>
        </div>

        <div className='btnWrap'>
          <IonButton color='primary' shape='round'>
            Upload Payment Slip
          </IonButton>
          <Typography>
            Payment status will be updated within 48 hours after validation
            process
          </Typography>
        </div>

      </div>
    </AuthPage>
    </>
  );
};
