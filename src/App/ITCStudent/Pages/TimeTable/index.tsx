import './timetable.scss';
import React,{useState,useEffect} from 'react';
import { AuthPage } from '../../../../components';
import { Typography } from '@mui/material';
import { style } from './../../../../Utility';
// import { SideMenu } from "App/ITCTeacher/Components";
import { SideMenu } from 'App/ITCStudent/Components';
import { getStudentTimetable } from 'api/api';

export const TimeTable = () => {

  const [studenttime,setstudenttime] = useState([])
    
  useEffect(()=>{
    getStudentTimetable().then(res => {
      setstudenttime(res.data.data);
    })
  },[])
  console.log(studenttime,"my student time table for student")
  

  return (
    <>
    <SideMenu/>
    
    <AuthPage containerWidth='lg' title='Time Table' showBackButton>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <div className='ion-padding' style={{overflow: "auto"}}>
          <p className='color-tertiary heading'>MONDAY (28/03/2022)</p>
          <div className='simpleTable timetable'>
            <table className='table'>
              <thead>
                {/* <tr>
                  <th></th>
                  <th>MONDAY</th>
                  <th>TUESDAY</th>
                  <th>WEDNESDAY</th>
                  <th>THURSDAY</th>
                  <th>FRIDAY</th>
                  <th>SATURDAY</th>
                </tr> */}
                <tr>
                  <th>Id</th>
                  <th>Standard</th>
                  <th>File Type</th>
                  <th>File Name</th>
                </tr>
              </thead>
              <tbody>
                
              {
               studenttime.length > 0 && studenttime.map((item,index) =>(
                <>
              <tr key={index}>
              <td>{item.id}</td>
              <td>{item.standard}</td>
              <td>{item.file_type}</td>
              <td>
                  <a href={item.file_name} target="_blank" style={{
                    color:'green',
                    textDecoration:'none'
                  }}>click this link</a>
              </td>
            </tr>
                </>
               ))
            }


                {/* <tr>
                  <td>6PM - 7PM</td>
                  <td>
                    <span
                      className='color-secondary'
                      style={{ textDecoration: 'underline' }}
                    >
                      SCIENCE (B.MELAYU)
                    </span>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>MATHS</td>
                  <td></td>
                </tr> */}
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </AuthPage>
    </>
  );
};
