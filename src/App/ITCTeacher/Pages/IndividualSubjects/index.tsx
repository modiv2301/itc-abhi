import { Stack, Typography } from "@mui/material";
import React from "react";
import { useHistory } from "react-router";
import { AuthPage, Awesome3DButton } from "../../../../components";
import { style } from "./../../../../Utility";
// import { SideMenu } from "App/ITCTeacher/Components";

export const IndividualSubjects = () => {
  const history = useHistory();
  return (
    <>
    {/* <SideMenu/> */}
    <AuthPage title="Individual Package" showBackButton>
      <div style={{ paddingTop: "5rem" }} className="ion-padding">
        <Stack spacing={5} sx={{ display: "flex", justifyContent: "center" }}>
          <div>
            <Awesome3DButton
              style={{
                width: style.Awesome3DButtonWidth,
                height: style.Awesome3DButtonHeight,
                fontSize: style.Awesome3DButtonTextSize,
              }}
              onClick={() => history.push("/t/package")}
            >
              Individual Package (30 DAYS)
            </Awesome3DButton>
            <Typography sx={{ textAlign: "center" }}>
              <div>
                <b style={{ fontSize: "x-large" }}>RM 40</b> / one subject.
              </div>
              <div>
                <b style={{ fontSize: "x-large" }}>RM 30</b> / one subject.
              </div>
              <div>Applies when enroll two or more subjects</div>
            </Typography>
          </div>
          <div>
            <Awesome3DButton
              style={{
                width: style.Awesome3DButtonWidth,
                height: style.Awesome3DButtonHeight,
                fontSize: style.Awesome3DButtonTextSize,
              }}
              onClick={() => history.push("/t/individual-package")}
            >
              Individual Package (15 DAYS)
            </Awesome3DButton>
            <Typography sx={{ textAlign: "center" }}>
              <div>
                <b style={{ fontSize: "x-large" }}>RM 20</b> / one subject.
              </div>
              <div>
                <b style={{ fontSize: "x-large" }}>RM 15</b> / one subject.
              </div>
              <div>Applies when enroll two or more subjects</div>
            </Typography>
          </div>
        </Stack>
      </div>
    </AuthPage>
    </>
  );
};
