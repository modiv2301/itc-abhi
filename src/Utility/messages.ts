export const messages = {
    required: {
        fname: "First Name is required",
        lname:"Last Name is required",
        full_name:"Full Name is required",
        mobile_no:"Mobile Number is required",
        password:"Password is required",
        confirm_password:"Confirm Password is required",
        parent_mobile_no: "Parents mobile number required"
    },
    regex:{
        name:"Enter Only characters and space is allowed",
        mobile_no:"Enter Only 10 digit number start with 6 or 7 or 8 or 9",
        password:"Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
    }
}