import { IonButton } from '@ionic/react';
import { Box, Typography } from '@mui/material';
import React from 'react';
import './classdetail.scss';
import { useHistory } from "react-router";
import { getStudentGenAtt } from 'api/api';


export interface ClassDetailsProps {
  subject: string;
  timing: string;
  classLink: string;
}

export const ClassDetails = (props: ClassDetailsProps) => {
  
  const history = useHistory();

  let profile = localStorage.getItem("profile")

  console.log("link profile" + JSON.parse(profile).id)

  const handelClick = (classLink) => {
  
    getStudentGenAtt(JSON.parse(profile).id).then(res => {
      window.location.href = classLink
      // console.log("jump link" + JSON.stringify(res))
    })
  }
  
  

  return (
    <Box className='classDetailCard'>
      <Typography sx={{ textAlign: 'center', fontWeight: 'bold' }}>
        {props.subject}
      </Typography>
      <Typography
        sx={{ textAlign: 'center', color: 'black', fontSize: 'small' }}
      >
        Time: {props.timing}
      </Typography>
      <div
        style={{ display: 'flex', justifyContent: 'space-between' }}
        className='ctaBtnWrapper'
      >
        
          <IonButton onClick={()=>handelClick(props.classLink)} shape='round' color='secondary' size='small'>
          LINK
        </IonButton>
          
        <IonButton shape='round' color='primary' size='small'>
          NOTES
        </IonButton>
      </div>
      
    </Box>
  );
};
