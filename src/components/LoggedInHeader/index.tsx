import { IonButton, IonIcon } from '@ionic/react';
import { addCircleSharp, menuSharp } from 'ionicons/icons';
import React from 'react';
import { useHistory } from 'react-router';
import { Header } from '../Header';

export const LoggedInHeader = () => {
    const history = useHistory();
    return (
        <Header
            icon={menuSharp}
            onIconClick={() => console.log("OpEN MenU")}
            rightAlignIcon={<IonButton shape="round" size="small">Cash <IonIcon icon={addCircleSharp} /> </IonButton>}
            rightAlignIconOnClick={() => history.push('/add-cash')}
        />
    )
};