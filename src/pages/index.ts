export { Login } from './Login';
export { Signup } from './Signup';
export { ForgotPassword } from './ForgotPassword';
export { StudentInfo } from './StudentInfo';
export { EnterOtp } from './EnterOtp';
export { NewPassword } from './NewPassword';
export { ParentInfo } from './ParentsProfile';
export { Registeration } from './Registeration';
export { ITCTeacher } from './../App/ITCTeacher';
export { ITCStudent } from './../App/ITCStudent';
