import { Dispatch } from "redux";

export const STORE_FIREBASE_TOKEN = "FIREBASE_TOKEN";

export const setfirebasetoken = (token: string) => (dispatch: Dispatch) => {
  console.log("token");
  dispatch({
    type: STORE_FIREBASE_TOKEN,
    payload: {
      token,
    },
  });
};
