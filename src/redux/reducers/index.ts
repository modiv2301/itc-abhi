import { combineReducers } from "redux";
import { dashboard } from "./dashboard";
import { common } from "./common";

export default combineReducers({ dashboard, common});
