import { requestForToken } from "firebase";
import { storetoken } from "redux/constants";

const initialState = {
  firebasetoken: "kjbhhjg",
};

interface ReduxActionTypeProps {
  type: string;
  token: string;
  payload: any;
}

export const token = (state = initialState, action: ReduxActionTypeProps) => {
  console.log("token action", action);
  switch (action.type) {
    case storetoken.STORE_FIREBASE_TOKEN:
      return {
        ...state,
        firebasetoken: action.payload.token,
      };
    default:
      return {
        ...state,
      };
  }
};
